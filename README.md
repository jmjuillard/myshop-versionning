# myShop-versionning

* _Binome 1_ Camille Croci-Torti _Binome 2_ Jean-Michel juillard
* etapes realisees de 1 a 6.9
* pas d'etapes non realisees
* repartition des taches dans le binome
    * partie 2 faite par binome 2 
    * partie 3 faite en parallele par les deux binomes
    * partie 4.1 faite par les deux binomes
    * partie 4.2 faite par les deux binomes selon repartition de l'enonce
    * partie 5 repartie entre les deux binomes
    * parties 6.1 a 6.4 et 6.9 realisees par binome 2
    * parties 6.5 a 6.8 realisees par binome 1
* Classes couvertes par des tests unitaires
    * Item
    * Shop
