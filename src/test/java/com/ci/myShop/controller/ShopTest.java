package com.ci.myShop.controller;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.awt.List;

import org.junit.Test;

import com.ci.myshop.controller.Shop;
import com.ci.myshop.controller.Storage;
import com.ci.myshop.model.Item;

public class ShopTest {
	
	@Test
	public void sellOperation() {


		Item itemA = new Item("OLED", 16, 250, 3);
		Item itemB = new Item("IPAD", 12, 300, 7);
		
		
		List list = new List();
		Storage stock = new Storage();
		stock.addItem(itemA);
		stock.addItem(itemB);
		
		
		Shop shop = new Shop(stock,  450);
		
		//récupere cash
		assertEquals(450, shop.getCash (),0);
		
		//récupere nbre de TV OLED
		assertEquals(3,itemA.getNbrElt());
		
		shop.Sell("OLED");
				
		
		// comparaison cash avant et apres
		assertEquals(700, shop.getCash(),0);
		
		// nbre de OLED avant operation -1 = nbre de OLED apres operation
		assertEquals(2, itemA.getNbrElt());
		
		
		
		

	}

}
