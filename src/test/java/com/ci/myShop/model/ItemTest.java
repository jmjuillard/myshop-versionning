package com.ci.myShop.model;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import com.ci.myshop.model.Item;

public class ItemTest {
	
	@Test
	public void setName() {
		Item item = new Item();
		item.setName("doe");
		assert("doe".equals(item.getName()));
		
	}

}
