package com.ci.myshop.controller;

import java.util.ArrayList;

import com.ci.myshop.model.Book;
import com.ci.myshop.model.Consumable;
import com.ci.myshop.model.Item;

public class Shop {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	private Storage storage;
	private float cash;
	
	public float getCash() {
	       return cash;
	   }
	public Shop(Storage storage, float cash) {
		this.storage = storage;
		this.cash = cash;
	}
	
	public Item Sell (String Name) {
		if (this.storage.getItem(Name).getName().equals("")){
			System.out.println ("erreur");
			return null;
		}
		this.storage.getItem(Name).setNbrElt((this.storage.getItem(Name).getNbrElt())-1);
		this.cash += this.storage.getItem(Name).getPrice();
		return this.storage.getItem(Name);
	}
	public boolean ItemBuy (Item object, Object ArrayList) {
		float prix = object.getPrice();
		if (prix> this.cash) 
			return false;
		else {
			this.cash = this.cash - prix;
			return true;
		}	
	}
	 public boolean isItemAvailable (String name) {
		       System.out.println(this.storage.getItem(name).getNbrElt());
		       if (this.storage.getItem(name).getNbrElt() > 0)
		           return true;
		       else
		           return false;
		   }
	 public int getAgeForBook(String name) {
		       if (this.storage.getItem(name) instanceof Book) {
		           return ((Book) this.storage.getItem(name)).getAge();
		       }
		       else
		           return -1;
		   }
	 public ArrayList<Book> getAllBook() {
		       ArrayList<Book> result = new ArrayList<Book>();
		       for (String  newName : this.storage.getItemMap().keySet()){
		           if (this.storage.getItemMap().get(newName) instanceof Book)
		               result.add((Book)this.storage.getItemMap().get(newName));
		       }
		       return result;
		   }
	public int getNbItemInStorage(String name) {
		       return this.storage.getItem(name).getNbrElt();
		   }
	public int getQuantityPerConsumable(Consumable name) {
		       if (name instanceof Consumable) {
		           return name.getQuantity();
		       }
		       else
		           return -1;
		   }	
	}

			
	
	
				
	


