package com.ci.myshop.controller;

import java.util.HashMap;
import java.util.Map;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.ci.myshop.model.Item;

public class Storage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	private HashMap<String, Item>itemMap;
	
	/**
	 * @return the itemMap
	 */
	public HashMap<String, Item> getItemMap() {
		return itemMap;
	}

	/**
	 * @param itemMap the itemMap to set
	 */
	public void setItemMap(HashMap<String, Item> itemMap) {
		this.itemMap = itemMap;
	}

	public Storage (){
		this.itemMap = new HashMap<String, Item>();
		
	}
	
	public void addItem(Item obj) {
		this.itemMap.put(obj.getName(), obj);
		
		
	}
	public Item getItem (String name) {
		if (this.itemMap.containsKey(name))
		return this.itemMap.get(name);
		
		
		else 
			return new Item();
			
	}


}
