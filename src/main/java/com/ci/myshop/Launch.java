
package com.ci.myshop;

import java.awt.List;
import java.util.ArrayList;

import com.ci.myshop.controller.Shop;
import com.ci.myshop.controller.Storage;
import com.ci.myshop.model.Item;


public class Launch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
	ArrayList<Item> listItem = new ArrayList<Item>();
	
	Item velo = new Item("MoutainBike",30, 120, 10);
    Item tv = new Item("LCD",42, 200, 12);
    listItem.add(velo);
    Storage wharehouse = new Storage();
    wharehouse.addItem(velo);
    Shop magasin = new Shop (wharehouse, 200);
    
    System.out.println("Rachat d'un objet : " + magasin.ItemBuy(tv,listItem));
    System.out.println("Cash : " + magasin.getCash());
    Item vendu = magasin.Sell(tv.getName());
    System.out.println("Vente du produit : " + vendu.display());
    System.out.println("Cash restant : " + magasin.getCash());
    
	
}
}
	

