package com.ci.myshop.model;

public class Book extends Item {


public static void main(String[] args) {
	// TODO Auto-generated method stub
		
}
private int nbPage;
private String author;
private String publisher;
private int year;
private int age;
/**
 * @return the nbPage
 */
public int getNbPage() {
	return nbPage;
}
/**
 * @param nbPage the nbPage to set
 */
public void setNbPage(int nbPage) {
	this.nbPage = nbPage;
}
/**
 * @return the author
 */
public String getAuthor() {
	return author;
}
/**
 * @param author the author to set
 */
public void setAuthor(String author) {
	this.author = author;
}
/**
 * @return the publisher
 */
public String getPublisher() {
	return publisher;
}
/**
 * @param publisher the publisher to set
 */
public void setPublisher(String publisher) {
	this.publisher = publisher;
}
/**
 * @return the year
 */
public int getYear() {
	return year;
}
/**
 * @param year the year to set
 */
public void setYear(int year) {
	this.year = year;
}
/**
 * @return the age
 */
public int getAge() {
	return age;
}
/**
 * @param age the age to set
 */
public void setAge(int age) {
	this.age = age;
}




}
