package com.ci.myshop.model;

public class Item {

	
		// TODO Auto-generated constructor stub
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	private String name;
	private int id;
	private float price;
	private int nbrElt;
	
	public Item() {
		
	}
	
	
	public Item(String n, int i, float j, int k) {
		name=n;
		id=i;
		price=j;
		nbrElt=k;
	}
	
	/**
	 * @return the nameString
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}
	/**
	 * @return the nbrElt
	 */
	public int getNbrElt() {
		return nbrElt;
	}
	/**
	 * @param nbrElt the nbrElt to set
	 */
	public void setNbrElt(int nbrElt) {
		this.nbrElt = nbrElt;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(float price) {
		this.price = price;
	}
	public String display() {
		String affichage = "";
		affichage = String.format("name" + this.getName() + "id" + this.getId()+ "Price" + this.getPrice() + "quantit�" + this.getNbrElt());
		return affichage;
		
	}
	
	

}
