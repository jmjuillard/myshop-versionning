package com.ci.myshop.model;

public class Pen extends Consumable{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	private String colorString;
	private int durability;
	/**
	 * @return the colorString
	 */
	public String getColorString() {
		return colorString;
	}
	/**
	 * @param colorString the colorString to set
	 */
	public void setColorString(String colorString) {
		this.colorString = colorString;
	}
	/**
	 * @return the durability
	 */
	public int getDurability() {
		return durability;
	}
	/**
	 * @param durability the durability to set
	 */
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	

}
